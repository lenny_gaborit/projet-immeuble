# **PROJET IMMEUBLES**

## Le projet :
    - L'objectif était de crée un programme python et à l'aide du module turtle de dessiner une suite d'immeubles avec plusieurs spécificitées comme par exemple des fenêtres carré de 30 pixels.

## Comment je m'y suis prit 
    - Tout d'abord je me suis renseigné sur le module turtle et ensuite je me suis lancé en python 
### Explication de mon code python 
- J'ai commencé par importer les modules dont j'avais besoin 

    ```python
    import turtle as trt
    import math
    import random 
    from random import randint as rdt
    ```
- Ensuite j'ai définit quelques valeurs qu'il fallait respectées dans l'énoncé
    ```python
    fenêtre = 30    
    Largeur_Pfenetre = 30
    Longueur_Pfenetre = 50
    Largeur_porte = 30
    Longueur_porte = 50
    Largeur_toit = 10
    Longueur_toit = 140
    ```
- J'ai crée une fonction pour dessiner une fenêtre blanche de 30 pixels
    ```python
    def fenetres(fenêtres):
        trt.forward(12)
        trt.fillcolor('white')
        trt.begin_fill()
        trt.down()
        for i in range(4):
            trt.forward(fenêtre)
            trt.right(90)
        trt.end_fill()
        trt.up()
        trt.forward(fenêtre)
    ```
- J'ai crée une fonction pour dessiner une porte fenêtre 
    ```python
    def p_fenetre(long, larg):
    trt.forward(12)
    trt.fillcolor('white')
    trt.begin_fill()
    for i in range(2):
        trt.down()
        trt.forward(larg)
        trt.right(90)
        trt.forward(long)
        trt.right(90)
    trt.end_fill()
    """ la tortue se déplace pour pouvoir tracer les barrieres du balcon """
    trt.up()
    trt.right(90)
    trt.forward(long-long/3)
    trt.right(90)
    trt.forward(larg/5)
    trt.right(180)
    balcon(long/3, larg/5)
    ```
- Ensuite j'ai crée une fonction qui appel aléatoirement la fonction fenêtre ou porte fenêtre
    ```python
    def fenetre_p_fenetre(nb):
        for i in range(nb):
            variable = rdt(0,2)
            if variable <1:
                p_fenetre(Longueur_Pfenetre, Largeur_Pfenetre)
            else:
                fenetres(fenêtre)
    ```
- J'ai crée une fonction qui dessine les balcons 
    ```python
    def balcon(longueur, largeur):
        trt.down()
        for i in range(7):
            for j in range(2):
                trt.forward(largeur)
                trt.right(90)
                trt.forward(longueur)
                trt.right(90)
            trt.forward(largeur)
        trt.backward(largeur)
        trt.left(90)
        trt.forward(longueur*2)
        trt.right(90)
        trt.up()
    ```
- Ensuite j'ai crée une fonction pour dessiner une porte
    ```python
    def porte(long, larg):
    trt.fillcolor('black')
    trt.forward(12)
    trt.begin_fill()
    for i in range(2):
        trt.down()
        trt.forward(larg)
        trt.right(90)
        trt.forward(long)
        trt.right(90)
    trt.end_fill()
    trt.up()
    trt.forward(larg)
    ```
- J'ai crée une fonction pour dessiner un toit 
    ```python
    def toit1(long, larg):
        trt.fillcolor('black')
        trt.begin_fill()
        for i in range(2):
            trt.down()
            trt.forward(long)
            trt.left(90)
            trt.forward(larg)
            trt.left(90)
        trt.end_fill()
        trt.up()
        trt.forward(long)
    ```
- Et une deuxieme pour un deuxieme toit différent
    ```python
    def toit2(long, larg):
        trt.fillcolor('black')
        trt.backward(5)
        trt.begin_fill()
        for i in range(2):
            trt.down()
            trt.forward(long)
            trt.left(90)
            trt.forward(larg)
            trt.left(90)
        trt.end_fill()
        trt.up()
        trt.forward(long)
    ```
- Ensuite une fonction qui choisis aléatoirement entre les deux toits
    ```python
    def choix_toit():
        variable = rdt(0,1)
        if variable1 == 0 :
            toit2(Longueur_toit2,Largeur_toit2)
        else:
            toit1(Longueur_toit1,Largeur_toit1)
    ```
- Puis une fonction qui definit une couleur aléatoire pour ensuite l'utiliser pour définir la couleur de l'immeuble 
    ```python
    def couleurs():
        color = []
        for i in range(3):
            color.append(random.random())
        return color
    ```
- Après j'ai crée une fonction qui dessine un étage 
    ```python
    def etages(couleur):
    trt.fillcolor(couleur)
    trt.begin_fill()
    for i in range(2):
        trt.down()
        trt.forward(140)
        trt.left(90)
        trt.forward(60)
        trt.left(90)
    trt.end_fill()
    trt.up()
    trt.left(90)
    trt.forward(50)
    trt.right(90)

    fenetre_p_fenetre(3)

    trt.forward(14)
    trt.left(90)
    trt.forward(10)
    trt.left(90)
    trt.forward(140)
    trt.right(180)
    ```
- De plus j'ai crée une fonction qui crée le rez de chaussée car il est différent d'un étage car il contient une porte
    ```python
    def rez_de_chaussé(couleur):
    trt.fillcolor(couleur)
    trt.begin_fill()
    for i in range(2):
        trt.down()
        trt.forward(140)
        trt.left(90)
        trt.forward(60)
        trt.left(90)
    trt.end_fill()
    trt.up()
    trt.left(90)
    trt.forward(50)
    trt.right(90)

    emplacement = rdt(0,2)
    if emplacement == 0:
        porte(Longueur_porte, Largeur_porte)
        fenetre_p_fenetre(2)
    elif emplacement == 1:
        fenetre_p_fenetre(1)
        porte(Longueur_porte, Largeur_porte)
        fenetre_p_fenetre(1)
    else:
        fenetre_p_fenetre(2)
        porte(Longueur_porte, Largeur_porte)

    trt.forward(14)
    trt.left(90)
    trt.forward(10)
    trt.left(90)
    trt.forward(140)
    trt.right(180)
    ```
- Enfin je crée une fonction qui utilise tout les autres petites fonctions pour dessiner les immeubles 
    ```python
        def immeubles():
            trt.ht()
            trt.speed(0)
            trt.up()
            trt.goto(-300, -150)
            trt.down()
            for i in range(4):
                couleur = couleurs()
                rez_de_chaussé(couleur)
                nbr_etages = rdt(0,4)
                for j in range(nbr_etages):
                    etages(couleur)
                choix_toit()
                trt.right(90)
                trt.forward((nbr_etages+1)*60)
                trt.left(90)
                trt.forward(20)
    ```

### Pour utiliser le code télécharger le deuxieme fichier du dossier et lancer le sur Visual studio ###