import turtle as trt
import math
import random 
from random import randint as rdt

fenêtre = 30    
Largeur_Pfenetre = 30
Longueur_Pfenetre = 50
Largeur_porte = 30
Longueur_porte = 50
Largeur_toit1 = 10
Longueur_toit1 = 140
Largeur_toit2=30
Longueur_toit2=150

def couleurs():
    """
    assigne à "farben" une couleur aléatoire
    """
    color = []
    for i in range(3):
        color.append(random.random())
    return color

def fenetres(fenêtres):
    """
    Trace un carré de côté "cotes"
    """
    trt.forward(12)
    trt.fillcolor('white')
    trt.begin_fill()
    trt.down()
    for i in range(4):
        trt.forward(fenêtre)
        trt.right(90)
    trt.end_fill()
    trt.up()
    trt.forward(fenêtre)

def p_fenetre(long, larg):
    trt.forward(12)
    trt.fillcolor('white')
    trt.begin_fill()
    for i in range(2):
        trt.down()
        trt.forward(larg)
        trt.right(90)
        trt.forward(long)
        trt.right(90)
    trt.end_fill()
    trt.up()
    trt.right(90)
    trt.forward(long-long/3)
    trt.right(90)
    trt.forward(larg/5)
    trt.right(180)
    balcon(long/3, larg/5)

def balcon(longueur, largeur):
    trt.down()
    for i in range(7):
        for j in range(2):
            trt.forward(largeur)
            trt.right(90)
            trt.forward(longueur)
            trt.right(90)
        trt.forward(largeur)
    trt.backward(largeur)
    trt.left(90)
    trt.forward(longueur*2)
    trt.right(90)
    trt.up()

def porte(long, larg):
    """
    Trace un rectangle de longueur "long" et de largeur "larg"
    """
    trt.fillcolor('black')
    trt.forward(12)
    trt.begin_fill()
    for i in range(2):
        trt.down()
        trt.forward(larg)
        trt.right(90)
        trt.forward(long)
        trt.right(90)
    trt.end_fill()
    trt.up()
    trt.forward(larg)

def toit1(long, larg):
    """
    Trace un rectangle de longueur "long" et de largeur "larg"
    """
    trt.fillcolor('black')
    trt.begin_fill()
    for i in range(2):
        trt.down()
        trt.forward(long)
        trt.left(90)
        trt.forward(larg)
        trt.left(90)
    trt.end_fill()
    trt.up()
    trt.forward(long)

def toit2(long, larg):
    trt.fillcolor('black')
    trt.backward(5)
    trt.begin_fill()
    for i in range(2):
        trt.down()
        trt.forward(long)
        trt.left(90)
        trt.forward(larg)
        trt.left(90)
    trt.end_fill()
    trt.up()
    trt.forward(long)

def fenetre_p_fenetre(nb):
    for i in range(nb):
        variable = rdt(0,2)
        if variable <1:
            p_fenetre(Longueur_Pfenetre, Largeur_Pfenetre)
        else:
            fenetres(fenêtre)

def choix_toit():
    variable = rdt(0,1)
    if variable1 == 0 :
        toit2(Longueur_toit2,Largeur_toit2)
    else:
        toit1(Longueur_toit1,Largeur_toit1)
    
def etages(couleur):
    """
    Trace un rectangle de la couleur donnée en argument
    """
    trt.fillcolor(couleur)
    trt.begin_fill()
    for i in range(2):
        trt.down()
        trt.forward(140)
        trt.left(90)
        trt.forward(60)
        trt.left(90)
    trt.end_fill()
    trt.up()
    trt.left(90)
    trt.forward(50)
    trt.right(90)

    fenetre_p_fenetre(3)

    trt.forward(14)
    trt.left(90)
    trt.forward(10)
    trt.left(90)
    trt.forward(140)
    trt.right(180)

def rez_de_chaussé(couleur):
    """
    Trace un rectangle de la couleur donnée en argument avec des fenêtres et/ou des portes fenêtres et une porte
    """
    trt.fillcolor(couleur)
    trt.begin_fill()
    for i in range(2):
        trt.down()
        trt.forward(140)
        trt.left(90)
        trt.forward(60)
        trt.left(90)
    trt.end_fill()
    trt.up()
    trt.left(90)
    trt.forward(50)
    trt.right(90)

    emplacement = rdt(0,2)
    if emplacement == 0:
        porte(Longueur_porte, Largeur_porte)
        fenetre_p_fenetre(2)
    elif emplacement == 1:
        fenetre_p_fenetre(1)
        porte(Longueur_porte, Largeur_porte)
        fenetre_p_fenetre(1)
    else:
        fenetre_p_fenetre(2)
        porte(Longueur_porte, Largeur_porte)

    trt.forward(14)
    trt.left(90)
    trt.forward(10)
    trt.left(90)
    trt.forward(140)
    trt.right(180)

def immeubles():
    trt.ht()
    trt.speed(0)
    trt.up()
    trt.goto(-300, -150)
    trt.down()
    for i in range(4):
        couleur = couleurs()
        rez_de_chaussé(couleur)
        nbr_etages = rdt(0,4)
        for j in range(nbr_etages):
            etages(couleur)
        choix_toit()
        trt.right(90)
        trt.forward((nbr_etages+1)*60)
        trt.left(90)
        trt.forward(20)

immeubles()